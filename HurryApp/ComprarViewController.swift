//
//  ComprarViewController.swift
//  HurryPrint
//
//  Created by Emmanuel Valentín Granados López on 23/11/15.
//  Copyright © 2015 DevWorms. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ComprarViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIDocumentPickerDelegate, UIDocumentMenuDelegate, UIDocumentInteractionControllerDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate {
    
    //variables desde otros controllers
    var noSucursal = ""
    var costoFolders = 0.0
    var noEngargolado = 0.0
    var foldersRespuesta: [String] = ["0","0","0","0","0","0","0","0","0"]
    var engargoladosRespuesta: [String] = ["0","0","0","0","0","0","0","0","0","0"]
    
    @IBOutlet weak var tableViewComprar: UITableView!
    @IBOutlet weak var nameDoc: UIButton!
    
    var hurryPrintMethods = ConnectionHurryPrint()
    
    var textFields: [UITextField] = []
    var switches: [UISwitch] = []
    var switchesRespuesta: [String] = ["1","","","1","",""]
    var deleteUrl = true
    
    var dispBlancoNegro = true
    var dispColor = true
    
    var precioImpresionBN = 0.0
    var precioImpresionColor = 0.0
    
    var costoEngargolado = 0.0
    
    var popViewController : ExportarViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "fondo.png")!)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(ComprarViewController.swipeKeyBoard(_:)))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        swipeDown.delegate = self
        self.tableViewComprar.addGestureRecognizer(swipeDown)
        
        let precioTiendaBN = 0.95
        let precioTiendaColor = 4.95
        let precioDevworms = 0.05
        
        self.precioImpresionBN = precioTiendaBN + precioDevworms
        self.precioImpresionColor = precioTiendaColor + precioDevworms
        
        self.nameDoc.setTitle(MyFile.Name, for: UIControlState())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func validate(_ value: String) -> Bool {
        // "\\d+-\\d+" para digito(mas) - digito (mas)
        // "^[1-9]\\d*"// que empiece desde el 1 al 9 y q pueda tener mas digitos o ninguno mas
        let param = "^[1-9]\\d*|\\d+-\\d+"
        let test = NSPredicate(format: "SELF MATCHES %@", param)
        let result =  test.evaluate(with: value)
        
        return result
    }
    
    @IBAction func mandarPHP(_ sender: AnyObject) {
        
        let data = try? Data(contentsOf: URL(fileURLWithPath: MyFile.Path) )
        
        if (data == nil) {
            let alert = UIAlertView(title: "Nos faltó algo", message: "Selecciona un archivo existente", delegate: nil, cancelButtonTitle: "Ok")
            alert.show()
                
            return
                
        } else if ( !validate( textFields[0].text! ) ) {
            let alert = UIAlertView(title: "Nos faltó algo", message: "¿Cuántas hojas imprimiremos?", delegate: nil, cancelButtonTitle: "Ok")
            alert.show()
            
            return
            
        } else if (textFields[1].text! != ""){
            if !validate( textFields[1].text! ) {
                let alert = UIAlertView(title: "Error en Intervalo", message: "Asegúrate de escribir correctamente #-#.", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
                
                return
            }
        }
        
        if (textFields[2].text! != ""){
            if (!validate( textFields[2].text! )) {
                let alert = UIAlertView(title: "Error en Juegos", message: "Asegúrate de escribir correctamente #.", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
                
                return
            }
        }
        
        print("siguió")
        
        // se ejecuta el metodo y obtiene valores para mostrar
        let precioFinalEnviar = self.calcularTotalImpresion()
        
        let message = textoConfirmación(precioFinalEnviar)
        
        let alertController = UIAlertController(title: "Resumen de compra", message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Confirmar", style: .default) {
            UIAlertAction in
            
            self.mandarPostPHP(precioFinalEnviar)
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel) {
            UIAlertAction in
            return
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func textoConfirmación(_ precioFinalEnviar: String) -> String {
        let mensaje = "Tu impresión total será: $" + precioFinalEnviar + "\n" +
                        "Costo por Folders: $\(self.costoFolders) \n" +
                        "Costo por Engargolado: $\(self.costoEngargolado) \n\n" +
                        "Revisa tu compra."
        return mensaje
    }
    
    func mandarPostPHP(_ precioFinalEnviar: String) {
        let alert = UIAlertController(title: nil, message: "Se están enviando tus archivos...", preferredStyle: .alert)
        alert.view.tintColor = UIColor.black
        
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        
        let callAction = UIAlertAction(title: "Cancelar", style: .default, handler: {
            action in
            
            self.hurryPrintMethods.cancelConnection()
            return
        })
        alert.addAction(callAction)
        
        alert.view.addSubview(loadingIndicator)
        self.present(alert, animated: true, completion: nil)
        
        // ver el status de todos los switches
        for index in 0...5 {
            if switches[index].isOn {
                switchesRespuesta[index] = "1"
            } else {
                switchesRespuesta[index] = ""
            }
        }
        
        let parameters = [
            "sucursal" : self.noSucursal,
            "hojas" : textFields[0].text!,
            "intervalo" : textFields[1].text!,
            "totalimpresion" : precioFinalEnviar,
            "blanconegro" : switchesRespuesta[0],
            "color" : switchesRespuesta[1],
            "caratula" : switchesRespuesta[2],
            "carta" : switchesRespuesta[3],
            "oficio" : switchesRespuesta[4],
            "lados" : switchesRespuesta[5],
            "juegos" : textFields[2].text!,
            
            //Folders
            "FolderBeige" : foldersRespuesta[0],
            "FolderAzul" : foldersRespuesta[1],
            "FolderRosa" : foldersRespuesta[2],
            "FolderVerde" : foldersRespuesta[3],
            "FolderGuinda" : foldersRespuesta[4],
            "FolderAzulIntenso" : foldersRespuesta[5],
            "FolderRojo" : foldersRespuesta[6],
            "FolderNegro" : foldersRespuesta[7],
            "FolderMorado" : foldersRespuesta[8],
            
            //Engargolados
            "pGuinda" : engargoladosRespuesta[0],
            "pNegro" : engargoladosRespuesta[1],
            "pRojo" : engargoladosRespuesta[2],
            "pAzul" : engargoladosRespuesta[3],
            "pVerde" : engargoladosRespuesta[4],
            "pAmarillo" : engargoladosRespuesta[5],
            "pMorado" : engargoladosRespuesta[6],
            "pGris" : engargoladosRespuesta[7],
            "pBlanco" : engargoladosRespuesta[8],
            "pAzulFuerte" : engargoladosRespuesta[9],
            
            ]
        
        //print("json:")
        //print(parameters)
        
        //return
        
        //Completion Handler
        self.hurryPrintMethods.connectionRestApi( "http://hurryprint.devworms.com/class/SubirMovil.php", type: "POST1", headers: nil, parameters: parameters, completion: { (resultData) -> Void in
            
            self.dismiss(animated: false, completion: nil)
            
            self.parseJSON( resultData )
            
        })
    }
    
    func parseJSON(_ dataForJson: Data) {
        
        do {
            let json = try JSONSerialization.jsonObject(with: dataForJson, options: []) as! [String: AnyObject]
            
            if let estado = json["Estado"] as? Int {
                if estado == 1 {
                    
                    DispatchQueue.main.async(execute: {
                        
                        let alert = UIAlertView(title: "HurryPrint!", message: "Lánzate a la sucursal por tus impresiones.", delegate: nil, cancelButtonTitle: "OK")
                        alert.show()
                        
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                } else {
                    
                    DispatchQueue.main.async(execute: {
                        
                        let alert = UIAlertView(title: "Ocurrió algo", message: json["Descripcion"] as? String, delegate: nil, cancelButtonTitle: "OK")
                        alert.show()
                    })
                    
                }
            }
            
        } catch {
            print("error serializing JSON: \(error)")
            
            DispatchQueue.main.async(execute: {
                
                let alert = UIAlertView(title: "Ocurrió algo", message: "No pudimos enviar tu archivo, intentalo de nuevo, asegúrate de que todo este bien.", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            })
        }
        
    }
    
    func calcularTotalImpresion() -> String {

        var precioPorHojas = 0.0
        
        if self.switchesRespuesta[0] == "1" {
            precioPorHojas = Double(self.textFields[0].text!)! * self.precioImpresionBN
            
            if self.switchesRespuesta[2] == "1" { // impresiones con caratula a color
                precioPorHojas = (precioPorHojas - self.precioImpresionBN) + self.precioImpresionColor
            }
        }
        if self.switchesRespuesta[1] == "1" { // impresiones color
            precioPorHojas = Double(self.textFields[0].text!)! * self.precioImpresionColor
        }
        if textFields[2].text! != "" { // multiplicar por juegos
            precioPorHojas = precioPorHojas * Double(self.textFields[2].text!)!
        }
        
        if self.noEngargolado > 0.0 { // engargolado
            
            if ((Int(self.textFields[0].text!)) <= 40){
                self.costoEngargolado = ( 18.00 * self.noEngargolado )
            }else if ((Int(self.textFields[0].text!)) > 40 && (Int(self.textFields[0].text!)) <= 80){
                self.costoEngargolado = ( 20.00 * self.noEngargolado )
            }else if ((Int(self.textFields[0].text!)) > 80 && (Int(self.textFields[0].text!)) <= 110){
                self.costoEngargolado = ( 22.00 * self.noEngargolado )
            }else if ((Int(self.textFields[0].text!)) > 110 && (Int(self.textFields[0].text!)) <= 150){
                self.costoEngargolado = ( 24.00 * self.noEngargolado )
            }else if ((Int(self.textFields[0].text!)) > 150 && (Int(self.textFields[0].text!)) <= 200){
                self.costoEngargolado = ( 26.00 * self.noEngargolado )
            }else {
                let alert = UIAlertView(title: "Error en Engargolado", message: "No podremos engargolar.", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }
            precioPorHojas = precioPorHojas + self.costoEngargolado
        }
        
        // Folders
        precioPorHojas = precioPorHojas + self.costoFolders
        
        return String( precioPorHojas )
    }
 
    @IBAction func deleteFile(_ sender: AnyObject) {
 
        self.deleteDocFile()
    }
 
    func deleteDocFile() {
        MyFile.url = URL(fileURLWithPath:" ")
        self.nameDoc.setTitle("", for: UIControlState())
    }
    
    @IBAction func uploadFile(_ sender: AnyObject) {
        
        let documentMenu = UIDocumentMenuViewController(documentTypes: ["com.adobe.pdf", "com.microsoft.word.doc", "org.openxmlformats.wordprocessingml.document"], in: UIDocumentPickerMode.import)
            
        documentMenu.delegate = self
            
        //documentMenu.modalPresentationStyle = UIModalPresentationStyle.FullScreen
            
        documentMenu.addOption(withTitle: "Desde otra aplicación", image: nil, order: .first, handler: { () -> Void in
            self.openPopUpTutorial()
        })
            
            /*
            documentMenu.addOptionWithTitle("iPhone", image: nil, order: .First,
                    handler: {
                        
                        let fm = NSFileManager.defaultManager()
                        let url = NSBundle.mainBundle().resourcePath
                        
                        //
                        let dirPaths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,
                            .UserDomainMask, true)
                        let docsDir = dirPaths[0] as String
                        print(docsDir)
                        //
                        
                        if(url != nil ){
                            do {
                                let items = try fm.contentsOfDirectoryAtPath(url!)
                                
                                let pick = UIDocumentPickerViewController(URL: NSURL(fileURLWithPath: url!), inMode: UIDocumentPickerMode.ExportToService)
                                
                                pick.delegate = self
                                self.presentViewController(pick, animated: true, completion: nil)
                                
                                
                                for item in items {
                                    print("Found \(item)")
                                }
                                
                            } catch {
                                // failed to read directory – bad permissions, perhaps?
                                print("catch")
                            }
                        
                        }else { print("ni se pudo leer la ruta") }
                        
                        print("New Doc Requested") })
            */
            
        //ipad
        documentMenu.popoverPresentationController?.sourceView = self.view
            
        self.present(documentMenu, animated: true, completion: nil)
    }
    
    func openPopUpTutorial() {
        self.popViewController = storyboard!.instantiateViewController(withIdentifier: "ExportarViewController") as! ExportarViewController
        self.popViewController.showInView( self.view , animated: true, scaleX: 0.72, scaleY: 0.72)
    }
    
    @IBAction func openFile(_ sender: AnyObject) {
        
        if MyFile.Path != "" {
            let documentInteraction =  UIDocumentInteractionController(url: URL(fileURLWithPath: MyFile.Path) )
            documentInteraction.delegate = self
            
            deleteUrl = false
            
            // Preview PDF
            documentInteraction.presentPreview(animated: true)
            // menu abajo para abrir algun archivo
            //documentInteraction.presentOpenInMenuFromRect(sender.frame, inView: self.view, animated: true)
        }
        
    }
    
    // MARK: - UIDocumentInteractionControllerDelegate
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    // MARK: - UIDocumentPickerDelegate
    
    @available(iOS 8.0, *)
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        if controller.documentPickerMode == UIDocumentPickerMode.import {
            
            MyFile.url = url
            
            var fileSize : UInt64 = 0
            
            do {
                let attr : NSDictionary? = try FileManager.default.attributesOfItem( atPath: MyFile.Path ) as NSDictionary?
                
                if let _attr = attr {
                    fileSize = _attr.fileSize();
                    
                    print("fileSize: \(fileSize)")
                }
                
                self.nameDoc.setTitle(MyFile.Name, for: UIControlState())
                
            } catch {
                print("Error: \(error)")
            }
        }
    }
    
    // MARK: - UIDocumentMenuDelegate
    
    @available(iOS 8.0, *)
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    // MARK: - UITableViewDelegate, UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 12
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let stringIndex = String( indexPath.row )
        
        let cell = tableView.dequeueReusableCell( withIdentifier: stringIndex ) as UITableViewCell!
        
        switch (indexPath.row) {
            case 1,2,9:
                //print("This number is between 1,2,9")
                let txtF = cell?.viewWithTag( indexPath.row ) as! UITextField
                textFields += [txtF]
                txtF.delegate = self
            
            case 3...8:
                //print("This number is between 3 and 8")
                let swtch = cell?.viewWithTag( indexPath.row ) as! UISwitch
                switches += [swtch]
                swtch.addTarget(self, action: #selector(ComprarViewController.stateChanged(_:)), for: UIControlEvents.valueChanged)
            
                if indexPath.row == 3 {
                    swtch.setOn(dispBlancoNegro, animated: true)
                }
            
                if indexPath.row == 4 {
                    if dispBlancoNegro == true && dispColor == true {
                        swtch.setOn(false, animated: true)
                    } else {
                        swtch.setOn(dispColor, animated: true)
                    }
                    
                }
            
                //print("jum "+String(switches.count))
            
            default: break
        }
        
        return cell!
    }
    
    func stateChanged(_ switchState: UISwitch) {
        
        if switchState == switches[0] { // blanco negro
            if switchState.isOn {
                
                if dispBlancoNegro == false {
                    switches[0].setOn(false, animated: true)
                    
                    let alert = UIAlertView(title: "Sin disponibilidad", message: "No hay impresiones a B/N en esta sucursal.", delegate: nil, cancelButtonTitle: "OK")
                    alert.show()
                    
                    return
                }
                
                switches[1].setOn(false, animated: true)
                switches[2].setOn(false, animated: true)
            } else {
                
                if dispColor == false {
                    switches[0].setOn(true, animated: true)
                    
                    let alert = UIAlertView(title: "Sin disponibilidad", message: "No hay impresiones a color en esta sucursal.", delegate: nil, cancelButtonTitle: "OK")
                    alert.show()
                    
                    return
                }
                
                switches[1].setOn(true, animated: true)
                switches[2].setOn(false, animated: true)
            }
        } else if switchState == switches[1] { // Color
            if switchState.isOn {
                
                if dispColor == false {
                    switches[1].setOn(false, animated: true)
                    
                    let alert = UIAlertView(title: "Sin disponibilidad", message: "No hay impresiones a color en esta sucursal.", delegate: nil, cancelButtonTitle: "OK")
                    alert.show()
                    
                    return
                }
                
                switches[0].setOn(false, animated: true)
                switches[2].setOn(false, animated: true)
            } else {
                
                if dispBlancoNegro == false {
                    switches[1].setOn(true, animated: true)
                    
                    let alert = UIAlertView(title: "Sin disponibilidad", message: "No hay impresiones a B/N en esta sucursal.", delegate: nil, cancelButtonTitle: "OK")
                    alert.show()
                    
                    return
                }

                switches[0].setOn(true, animated: true)
                switches[2].setOn(false, animated: true)
            }
        } else if switchState == switches[2] { // Cáratula color
            if switchState.isOn {
                
                if dispColor == false || dispBlancoNegro == false {
                    switches[2].setOn(false, animated: true)
                    
                    let alert = UIAlertView(title: "Sin disponibilidad", message: "No hay impresiones a color o b/n en esta sucursal.", delegate: nil, cancelButtonTitle: "OK")
                    alert.show()
                    
                    return
                }
                
                switches[0].setOn(true, animated: true)
                switches[1].setOn(false, animated: true)
            } else {
                switches[0].setOn(true, animated: true)
                switches[1].setOn(false, animated: true)
            }
        } else if switchState == switches[3] { // Carta
            if switchState.isOn {
                switches[4].setOn(false, animated: true)
            } else {
                switches[4].setOn(true, animated: true)
            }
        } else if switchState == switches[4] { // Oficio
            if switchState.isOn {
                switches[3].setOn(false, animated: true)
            } else {
                switches[3].setOn(true, animated: true)
            }
        }
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //subir el table view para que se vea el campo textfield
        if textField == self.textFields[2] {
            self.tableViewComprar.setContentOffset(CGPoint(x: 0, y: 250), animated: true)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.textFields[2] {
            self.tableViewComprar.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
    }
    
    // MARK: - UIGestureRecognizerDelegate
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        //metodo para que detecte recognizer desde table view, ya que al parecer tiene scroll y recognizer propio
        return true
    }
    
    func swipeKeyBoard(_ sender:AnyObject) {
        //Baja el textField
        self.view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FolderSegue" {
            deleteUrl = false
        } else if segue.identifier == "EngargoladoSegue", let destination = segue.destination as? EngargoladoTableViewController {
            
            deleteUrl = false
            
            destination.juegosAimprimir = self.textFields[2].text!
        }
        
    }
    
    // for delete path or keep it when open another view
    override func viewDidDisappear(_ animated: Bool) {
        //print("ComprarViewController DidDisappear: "+animated.description)
        
        // when ComprarViewController has been eliminated from a view hierarchy
        if animated && deleteUrl {
            print("elimina url")
            self.deleteDocFile()
        }
        
        deleteUrl = true
    }
    
    @IBAction func unwindToImprimir(_ sender: UIStoryboardSegue)
    {
        //aqui manda el exit de otro view controller con (ctrl+obj)
        // Pull any data from the view controller which initiated the unwind segue.
    }

}
