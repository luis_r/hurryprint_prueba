//
//  SucursalesViewController.swift
//  HurryPrint
//
//  Created by Emmanuel Valentín Granados López on 15/01/16.
//  Copyright © 2016 DevWorms. All rights reserved.
//

import UIKit

/*
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}*/

class SucursalesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableViewSucursales: UITableView!
    
    var names = [String]()
    var blanco_negro = [String]()
    var color = [String]()
    var sucursalesAbiertas = [String]()
    var noSucursales = [String]()
    var numberRows = 0
    var horaDeDormir = false
    var hurryPrintMethods = ConnectionHurryPrint()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.calcularHora()
        
        //Completion Handler
        self.hurryPrintMethods.connectionRestApi( "http://hurryprint.devworms.com/api/sucursales", type: "GET", headers: nil, parameters: nil, completion: { (resultData) -> Void in
            
            self.parseJSON( resultData )
            
            DispatchQueue.main.async(execute: {
                
                self.tableViewSucursales.reloadData()
            })
        })
        
    }
    
    func calcularHora() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH"
        let hora: Int? = Int( dateFormatter.string(from: Date()) )
        dateFormatter.dateFormat = "mm"
        let minuto: Int? = Int( dateFormatter.string(from: Date()) )
        //dateFormatter.dateFormat = "a"
        //let period = dateFormatter.stringFromDate(NSDate())
        //print("hora: \(hora)"+", minuto: \(minuto)"+", period: \(period)")
        
        if hora! <= 6, minuto! <= 31 || hora! >= 21, minuto! >= 25 {
            
            self.horaDeDormir = true
            
            let alert = UIAlertView(title: "Impresión para mañana", message: "Puedes mandar ahora tus impresiones sin restricciones y el día de mañana recogerlas.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }
    
    func parseJSON(_ dataForJson: Data) {
        do {
            let json = try JSONSerialization.jsonObject(with: dataForJson, options: []) as! [String: AnyObject]
            
            if let sucursales = json["sucursal"] as? [[String: AnyObject]] {
                for sucursal in sucursales {
                    if let name = sucursal["id_tienda"] as? String {
                        self.noSucursales.append(name)
                    }
                    if let name = sucursal["nombre_tienda"] as? String {
                        self.names.append(name)
                    }
                    if let clr = sucursal["color"] as? String {
                        self.color.append(clr)
                    }
                    if let bn = sucursal["blanco_negro"] as? String {
                        self.blanco_negro.append(bn)
                    }
                    if let sucAbierta = sucursal["abierto"] as? String {
                        self.sucursalesAbiertas.append(sucAbierta)
                    }
                }
                
                if sucursales.count > 0 {
                    self.numberRows = sucursales.count
                    
                }
            }
        } catch {
            print("error serializing JSON: \(error)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDelegate, UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numberRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellSucursal", for: indexPath) as UITableViewCell
        
        let nameSucursal = cell.viewWithTag(1) as! UILabel
        nameSucursal.text = self.names[ indexPath.row ]
        
        let disponibilidad_sucAbierta = cell.viewWithTag(2)
        
        if self.sucursalesAbiertas[indexPath.row] != "1" {
            disponibilidad_sucAbierta?.backgroundColor = UIColor.red
        }
        
        let disponibilidad_b_n = cell.viewWithTag(3)
        
        if self.blanco_negro[indexPath.row] != "1" {
            disponibilidad_b_n?.backgroundColor = UIColor.red
        }
        
        let disponibilidad_color = cell.viewWithTag(4)
        
        if self.color[indexPath.row] != "1" {
            disponibilidad_color?.backgroundColor = UIColor.red
        }
        
        return cell
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "CompraSegue", let destination = segue.destination as? ComprarViewController {
            if let cell = sender as? UITableViewCell, let indexPath = self.tableViewSucursales.indexPath(for: cell) {
                
                destination.noSucursal = self.noSucursales[ indexPath.row ]
                
                if !self.horaDeDormir { // si estan abriertas las sucursales
                    
                    // disponibilidad en tienda
                    if self.blanco_negro[indexPath.row] == "1" {
                        destination.dispBlancoNegro = true
                    }else {
                        destination.dispBlancoNegro = false
                    }
                    
                    if self.color[indexPath.row] == "1" {
                        destination.dispColor = true
                    }else {
                        destination.dispColor = false
                    }
                } else {
                    
                    destination.dispBlancoNegro = true
                    destination.dispColor = true
                }
                
            }
        }
    }
    
    //Pasar a la siguiente pantalla o no
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        if let cell = sender as? UITableViewCell, let indexPath = self.tableViewSucursales.indexPath(for: cell) {
                
            if !self.horaDeDormir { // si estan abriertas las sucursales
                    
                // disponibilidad en tienda
                if ( (self.blanco_negro[indexPath.row] != "1") && (self.color[indexPath.row] != "1") ||
                        (self.sucursalesAbiertas[indexPath.row] != "1") ) {
                    
                    let alert = UIAlertView(title: "Sin disponibilidad", message: "No hay impresiones en esta sucursal.", delegate: nil, cancelButtonTitle: "OK")
                    alert.show()
                    
                    return false
                }
            }
        }
        
        return true
    }

}

//http://stackoverflow.com/questions/12301256/is-it-possible-to-set-uiview-border-properties-from-interface-builder
