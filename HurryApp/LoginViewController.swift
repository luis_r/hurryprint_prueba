//
//  LoginViewController.swift
//  HurryPrint
//
//  Created by Luis Enrique Ramírez on 11/07/17.
//  Copyright © 2017 DevWorms. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var btnEntrar: UIButton!
    @IBOutlet weak var btnFacebook: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        addShadowButtons()
    }

    func addShadowButtons(){
        btnEntrar.layer.shadowColor = UIColor.gray.cgColor
        btnEntrar.layer.shadowOffset = CGSize(width: 5, height: 5)
        btnEntrar.layer.shadowRadius = 5
        btnEntrar.layer.shadowOpacity = 1.0
        btnFacebook.layer.shadowColor = UIColor.gray.cgColor
        btnFacebook.layer.shadowOffset = CGSize(width: 5, height: 5)
        btnFacebook.layer.shadowRadius = 5
        btnFacebook.layer.shadowOpacity = 1.0
    }


}
