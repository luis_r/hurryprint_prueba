//
//  RegistroViewController.swift
//  HurryPrint
//
//  Created by Emmanuel Valentín Granados López on 22/11/15.
//  Copyright © 2015 DevWorms. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class RegistroViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var telefonoTxt: UITextField!
    @IBOutlet weak var contrasenaTxt: UITextField!
    @IBOutlet weak var confirmarTxt: UITextField!
    
    var hurryPrintMethods = ConnectionHurryPrint()
    func swipeKeyBoard(_ sender:AnyObject) {
        //Baja el textField
        self.view.endEditing(true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "fondo.png")!)
        
        self.telefonoTxt.delegate = self
        self.contrasenaTxt.delegate = self
        self.confirmarTxt.delegate = self
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(swipeKeyBoard(_:)))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
        
        self.name.text = FBSDKProfile.current().name
        
    }
    
    @IBAction func registrarseHurry(_ sender: AnyObject) {
        
        if !validate( self.telefonoTxt.text! ) {
            let alert = UIAlertView(title: "Error en teléfono", message: "Asegúrate de escribir correctamente tu teléfono.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
            
            return
        }
        
        if self.confirmarTxt.text != self.contrasenaTxt.text ||
           self.confirmarTxt.text == "" || self.contrasenaTxt.text == "" ||
            !validatePassword( self.contrasenaTxt.text! ) {
            
            let alert = UIAlertView(title: "Error en campos", message: "Asegúrate de escribir correctamente la contraseña, se requiere al menos 1 letra ó 1 número, no se aceptan carácteres especiales |!()-,*/?.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
            
            return
        }
        
        let headers = [
            "content-type": "application/json",
        ]
        
        let parameters = [
            "nombre": FBSDKProfile.current().name!,
            "contrasena": self.contrasenaTxt.text!,
            "telefono": self.telefonoTxt.text!,
            "token": FBSDKAccessToken.current().userID!
        ]
        
        //Completion Handler
        self.hurryPrintMethods.connectionRestApi( "http://hurryprint.devworms.com/api/usuarios/registro", type: "POST", headers: headers, parameters: parameters, completion: { (resultData) -> Void in
            
            self.parseJSON( resultData )
            
        })
    }
    
    func parseJSON(_ dataForJson: Data) {
        
        do {
            let json = try JSONSerialization.jsonObject(with: dataForJson, options: []) as! [String: AnyObject]
            
            if let registro = json["estado"] as? Int {
                if registro == 1 {
                
                    let apiKey = json["APIkey"] as! String
                    //saber si es la primera vez o no
                    let defaults = UserDefaults.standard
                    defaults.set(apiKey, forKey: "ApiKey")
                    defaults.set(FBSDKProfile.current().name!, forKey: "NombreUsuario")
                    
                    DispatchQueue.main.async(execute: { // swift 3, This application is modifying the autolayout engine from a background thread, which can lead to engine corruption and weird crashes.  This will cause an exception in a future release.
                        
                        let alert = UIAlertView(title: "", message: "Te regalamos SALDO para tus primeras impresiones, después podrás recargar en las sucursales o por PayPal*.", delegate: nil, cancelButtonTitle: "OK")
                        alert.show()
                        
                        self.performSegue(withIdentifier: "PrincipalSegue", sender: nil)
                    })
                    
                } else if registro == 3 {
                    
                    DispatchQueue.main.async(execute: { // swift 3, This application is modifying the autolayout engine from a background thread, which can lead to engine corruption and weird crashes.  This will cause an exception in a future release.
                        
                        let alert = UIAlertView(title: "Usuario ya registrado", message: "Ya te registraste anteriormente.", delegate: nil, cancelButtonTitle: "OK")
                        alert.show()
                    })
                    
                }
            }
            
        } catch {
            print("error serializing JSON: \(error)")
        }
        
    }
    
    func validate(_ value: String) -> Bool {
        let PHONE_REGEX = "^55\\d{8}"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        
        return result
    }
    
    func validatePassword(_ value: String) -> Bool {
        let PHONE_REGEX = "[a-zA-Z0-9]+"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        
        return result
    }


    
    override func willMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            
            //Apretó btn back
            if FBSDKProfile.current() != nil {
                FBSDKLoginManager().logOut()
            }
        }
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    

}
