//
//  PerfilViewController.swift
//  HurryPrint
//
//  Created by Emmanuel Valentín Granados López on 17/12/15.
//  Copyright © 2015 DevWorms. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class PerfilViewController: UIViewController, UIPopoverPresentationControllerDelegate {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var saldo: UILabel!
    @IBOutlet weak var saldoRegalo: UILabel!
    
    var hurryPrintMethods = ConnectionHurryPrint()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // NSNotificationCenter para pasarnos valores o observadores de que pasa en otras vistas
        NotificationCenter.default.addObserver(self, selector: #selector(PerfilViewController.refresh(_:)), name: NSNotification.Name(rawValue: "refreshSaldo"), object: nil)

        // Do any additional setup after loading the view.
        
        self.navigationItem.rightBarButtonItem?.target = self
        self.navigationItem.rightBarButtonItem?.action = #selector(PerfilViewController.refresh(_:))
        
        if FBSDKProfile.current() != nil {
            let imageFB = FBSDKProfilePictureView(frame: self.profileImage.frame)
            imageFB.profileID = FBSDKAccessToken.current().userID // "me"
            imageFB.layer.borderWidth = 1
            imageFB.layer.masksToBounds = false
            imageFB.layer.borderColor = UIColor.black.cgColor
            imageFB.layer.cornerRadius = self.profileImage.frame.height/2
            imageFB.clipsToBounds = true
            //imageFB.pictureMode = FBSDKProfilePictureMode.Normal
            self.view.addSubview(imageFB)
            
            self.profileName.text = FBSDKProfile.current().name
            
        } else {
            self.profileName.text = UserDefaults.standard.string(forKey: "NombreUsuario")!
            
            self.requestGraphAPIFB()
            
        }
        
        self.getSaldo()
    }
    
    func requestGraphAPIFB() {
        
        let idFb = UserDefaults.standard.string(forKey: "TokenFB")!
        
        let facebookProfileUrl = URL(string: "https://graph.facebook.com/\(idFb)/picture?type=large")
        
        if let data = try? Data(contentsOf: facebookProfileUrl!) {
            self.profileImage.image = UIImage(data: data)
            self.profileImage.layer.borderWidth = 1
            self.profileImage.layer.masksToBounds = false
            self.profileImage.layer.borderColor = UIColor.black.cgColor
            self.profileImage.layer.cornerRadius = self.profileImage.frame.height/2
            self.profileImage.clipsToBounds = true
        } else {
            print("no se pudo cargar la imagen")
        }
    }
    
    func getSaldo() {
        //Completion Handler
        self.hurryPrintMethods.connectionRestApi( "http://hurryprint.devworms.com/api/saldo", type: "GET", headers: nil, parameters: nil, completion: { (resultData) -> Void in
            
            self.parseJSON( resultData )
            
        })
    }
    
    func parseJSON(_ dataForJson: Data) {
        do {
            let json = try JSONSerialization.jsonObject(with: dataForJson, options: []) as! [String: AnyObject]
            
            if let saldo = json["saldo"]!["Saldo"] as? String {
                
                DispatchQueue.main.async(execute: {
                    
                    self.saldo.text = "$ " + saldo
                    self.saldoRegalo.text = "$ " + (json["saldo"]!["SaldoRegalo"] as? String)!
                })
            }
        } catch {
            print("error serializing JSON: \(error)")
        }
        
    }
    
    func refresh(_ sender:AnyObject) {
        self.getSaldo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func logOut(_ sender: AnyObject) {
        
        let defaults = UserDefaults.standard
        defaults.set("", forKey: "ApiKey")
        defaults.set("", forKey: "NombreUsuario")
        defaults.set("", forKey: "TokenFB")
        
        if FBSDKProfile.current() != nil {
            FBSDKLoginManager().logOut()
            
        }
        
        let vc = storyboard!.instantiateViewController(withIdentifier: "Inicio") 
        self.present( vc , animated: true, completion: nil)
    }

    @IBAction func showPopOver(_ sender: AnyObject) {
        
        let alert = UIAlertView(title: nil, message: "En contrucción.", delegate: nil, cancelButtonTitle: "OK")
        alert.show()
        
        // TODO
        // Pantalla para recargarte en PayPal
        //self.performSegueWithIdentifier("showViewPopover", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showViewPopover" {
            let vc = segue.destination as UIViewController
            
            let controller = vc.popoverPresentationController
            
            if controller != nil {
                controller?.delegate = self
            }
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}
