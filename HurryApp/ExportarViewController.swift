//
//  ExportarViewController.swift
//  HurryPrint
//
//  Created by Emmanuel Valentín Granados López on 18/01/16.
//  Copyright © 2016 DevWorms. All rights reserved.
//

import UIKit

class ExportarViewController: UIViewController, UIPageViewControllerDataSource {
    
    var popTransparentView: UIView!
    
    var pageViewController: UIPageViewController!
    var pageTitles: NSArray!
    var pageImages: NSArray!
    let myStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let pageController = UIPageControl.appearance()
        pageController.pageIndicatorTintColor = UIColor.lightGray
        pageController.currentPageIndicatorTintColor = UIColor.black
        pageController.backgroundColor = UIColor.white
        
        self.pageTitles = NSArray(objects: "Abrir otra aplicación", "Abrir el archivo", "Abrir opciones de compartir archivo", "Abrir en...", "Seleccionar HurryPrint", "Selecciona sucursal", "Archivo cargado" )
        self.pageImages = NSArray(objects: "i1","i2","i3","i4","i5","i6","i7")
        self.pageViewController = myStoryboard.instantiateViewController(withIdentifier: "PageViewController") as! UIPageViewController
        self.pageViewController.dataSource = self
        
        //posicionar en la primera
        let startVC = self.viewControllerAtIndex(0) as ContentPageViewController
        let viewControllers = NSArray(object: startVC)
        
        self.pageViewController.setViewControllers(viewControllers as? [UIViewController] , direction: .forward , animated: true, completion: nil)
        //
        
        self.pageViewController.view.frame = CGRect(x: 0, y: 5, width: self.view.frame.width, height: self.view.frame.height - 60)
        
        self.addChildViewController(self.pageViewController)
        
        self.view.addSubview(self.pageViewController.view)
        
        self.pageViewController.didMove(toParentViewController: self)
        
        //PopUp
        self.view.layer.cornerRadius = 5
        self.view.layer.shadowOpacity = 0.8
        self.view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.view.layer.masksToBounds = true
        self.view.layer.zPosition = 1;
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - PopUp

    @IBAction func closePopUp(_ sender: AnyObject) {
        
        self.removeAnimate()
    }
    
    func showInView(_ aView: UIView!, animated: Bool, scaleX: CGFloat, scaleY: CGFloat) {
        
        self.popTransparentView = UIView.init(frame:  aView.frame)
        self.popTransparentView.backgroundColor = UIColor(white: 0.5, alpha: 0.6)
        
        aView.addSubview( self.popTransparentView )
        aView.addSubview(self.view)
        
        if animated {
            
            self.showAnimate(scaleX, sY: scaleY)
        }
    }
    
    func showAnimate(_ sX: CGFloat, sY: CGFloat) {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: sX, y: sY)
        });
    }
    
    func removeAnimate() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished) {
                    
                    self.view.removeFromSuperview()
                    self.popTransparentView.removeFromSuperview()
                }
        });
    }
    
    func viewControllerAtIndex(_ index: Int) ->  ContentPageViewController {
        
        if( (self.pageTitles.count == 0) || (index >= self.pageTitles.count) ){
            return ContentPageViewController()
        }
        
        let vc: ContentPageViewController = myStoryboard.instantiateViewController(withIdentifier: "ContentPageViewController") as! ContentPageViewController
        
        vc.imageFile = self.pageImages[index] as! String
        vc.titleText = self.pageTitles[index] as! String
        vc.pageIndex = index
        
        return vc
        
    }

    // MARK: - UIPageViewControllerDataSource

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        let vc = viewController as! ContentPageViewController
        var index = vc.pageIndex as Int
        
        if (index == NSNotFound){
            return nil
        }
        
        index += 1
        
        if (index == self.pageTitles.count){
            return nil
        }
        
        return self.viewControllerAtIndex(index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let vc = viewController as! ContentPageViewController
        var index = vc.pageIndex as Int
        
        if (index == 0 || index == NSNotFound){
            return nil
        }
        
        index -= 1
        
        return self.viewControllerAtIndex(index)
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return self.pageTitles.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }

}
