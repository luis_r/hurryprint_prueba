//
//  ViewController.swift
//  HurryPrint
//
//  Created by Emmanuel Valentín Granados López on 22/11/15.
//  Copyright © 2015 DevWorms. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class ViewController: UIViewController, UITextFieldDelegate {
    /**
     Sent to the delegate when the button was used to login.
     - Parameter loginButton: the sender
     - Parameter result: The results of the login
     - Parameter error: The error (if any) from the login
     */
    /*public func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
     
    }*/

    
    /*var hurryPrintMethods = ConnectionHurryPrint()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(ViewController.swipeKeyBoard(_:)))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
        
        let loginButton = FBSDKLoginButton()
        loginButton.center.x = self.view.center.x
        loginButton.center.y = self.view.center.y - 20.0
        loginButton.setAttributedTitle( NSAttributedString(string: "Iniciar Sesión") , for: .normal)
        loginButton.delegate = self //important!
        self.view.addSubview(loginButton)
        
        let registrarButton = FBSDKLoginButton()
        registrarButton.center.x = self.view.center.x
        registrarButton.center.y = self.view.center.y + 20.0
        registrarButton.setAttributedTitle( NSAttributedString(string: "Registrarte") , for: .normal)
        registrarButton.delegate = self //important!
        self.view.addSubview(registrarButton)
        
        FBSDKProfile.enableUpdates(onAccessTokenChange: true)
        
    }

    func loginHurry() {
        
        let headers = [
            "content-type": "application/json",
        ]
        
        let parameters = [
            "token": FBSDKAccessToken.current().userID!
        ]
        
        //Completion Handler
        self.hurryPrintMethods.connectionRestApi( "http://hurryprint.devworms.com/api/usuarios/login", type: "POST", headers: headers, parameters: parameters, completion: { (resultData) -> Void in
            
            self.parseJSONlogin( resultData )
            
        })
    }
    
    func parseJSONlogin(_ data: Data) {
        //var datos = [[String : Any]]()
        do{
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            print(json)
            let registro = json["estado"] as? Int
            if registro! == 1 {
                let apiKey = json["usuario"]!["APIkey"] as! String
                let nombreUsuario = json["usuario"]!["Nombre"] as! String
                let tokenFBid = json["usuario"]!["Token"] as! String
                
                let defaults = UserDefaults.standard
                defaults.set(apiKey, forKey: "ApiKey")
                defaults.set(nombreUsuario, forKey: "NombreUsuario")
                defaults.set(tokenFBid, forKey: "TokenFB")
                
                DispatchQueue.main.async(execute: {
                    
                    self.performSegue(withIdentifier: "PrincipalSegue", sender: nil)
                })

            }
        } catch let err{
            print(err.localizedDescription)
        }
        //print(json)
        //let registro = json["estado"] as? Int
        
                    
            
                
                /*DispatchQueue.main.async {
                 //print(json)
                 
                 if datos.count > 0 {
                 datos.removeAll()
                 }
                 
                 if let jsonResult = json as? [String: Any] {
                 for dato in jsonResult["notificaciones"] as! [[String:Any]] {
                 datos.append(dato)
                 }
                 }*/
                
                //self.tableView.reloadData()
                
                
            }
        
        
        
        /*if let json = try? JSONSerialization.jsonObject( with: dataForJson , options: []) as! [String: Any] {
            if let registro = json["estado"] as? Int {
                if registro == 1 {
                    
                    let apiKey = json["usuario"]!["APIkey"] as! String
                    let nombreUsuario = json["usuario"]!["Nombre"] as! String
                    let tokenFBid = json["usuario"]!["Token"] as! String
                    
                    let defaults = UserDefaults.standard
                    defaults.set(apiKey, forKey: "ApiKey")
                    defaults.set(nombreUsuario, forKey: "NombreUsuario")
                    defaults.set(tokenFBid, forKey: "TokenFB")
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.performSegue(withIdentifier: "PrincipalSegue", sender: nil)
                    })
                    
                } else {
                    
                    DispatchQueue.main.async(execute: {
                        
                        let alert = UIAlertView(title: "Error en Login", message: json["mensaje"] as? String, delegate: nil, cancelButtonTitle: "OK")
                        alert.show()
                        
                        if FBSDKProfile.currentProfile() != nil {
                            FBSDKLoginManager().logOut()
                        }
                    })
                    
                }
            }

        }*/
        
        /*do {
            let json = try? JSONSerialization.jsonObject( with: dataForJson , options: [])
            
            if let registro = json["estado"] as? Int {
                if registro == 1 {
                    
                    let apiKey = json["usuario"]!!["APIkey"] as! String
                    let nombreUsuario = json["usuario"]!!["Nombre"] as! String
                    let tokenFBid = json["usuario"]!!["Token"] as! String
                    
                    let defaults = UserDefaults.standard
                    defaults.set(apiKey, forKey: "ApiKey")
                    defaults.set(nombreUsuario, forKey: "NombreUsuario")
                    defaults.set(tokenFBid, forKey: "TokenFB")
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.performSegue(withIdentifier: "PrincipalSegue", sender: nil)
                    })
                    
                } else {
                    
                    DispatchQueue.main.async(execute: {
                        
                        let alert = UIAlertView(title: "Error en Login", message: json["mensaje"] as? String, delegate: nil, cancelButtonTitle: "OK")
                        alert.show()
                        
                        if FBSDKProfile.currentProfile() != nil {
                            FBSDKLoginManager().logOut()
                        }
                    })
                    
                }
            }
            
        } catch {
            print("error serializing JSON: \(error)")
        }*/
        
    }
    
    func alertRegistro(_ email: String) {
        
        let mesage = "Confirma tu celular:" + "\n" +
                    "Recuerda que con este número podrás hacer recargas y acceder a beneficios HurryPrint"
        
        let alertController = UIAlertController(title: FBSDKProfile.currentProfile().name, message: mesage, preferredStyle: .Alert)
        
        let confirmAction = UIAlertAction(title: "Confirmar", style: .default) { (_) in
            if let field = alertController.textFields![0] as? UITextField {
                
                if !self.validate( field.text ) {
                    let alert = UIAlertView(title: "Error en teléfono", message: "Asegúrate de escribir correctamente tu teléfono.", delegate: nil, cancelButtonTitle: "OK")
                    alert.show()
                    
                    if FBSDKProfile.currentProfile() != nil {
                        FBSDKLoginManager().logOut()
                    }
                    
                    return
                }
                
                self.registrarseHurry(email, telefono: field.text!)
            
            } else {
                // user did not fill field
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel) {
            UIAlertAction in
            
            if FBSDKProfile.currentProfile() != nil {
                FBSDKLoginManager().logOut()
            }
        }
        
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "5510203040"
            textField.keyboardType = .NumberPad
            textField.textAlignment = .Center
        }
        
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func registrarseHurry(_ email: String, telefono: String) {
        
        
        
        let headers = [
            "content-type": "application/json",
            ]
        
        let parameters = [
            "nombre": FBSDKProfile.currentProfile().name!,
            "mail": email,
            "telefono": telefono,
            "token": FBSDKAccessToken.currentAccessToken().userID!
            ]
        
        //Completion Handler
        self.hurryPrintMethods.connectionRestApi( "http://hurryprint.devworms.com/api/usuarios/registro", type: "POST", headers: headers, parameters: parameters, completion: { (resultData) -> Void in
            
            self.parseJSONregistro( resultData )
            
        })
    }
    
    func parseJSONregistro(_ dataForJson: Data) {
        
        do {
            let json = try JSONSerialization.jsonObject( with: dataForJson , options: .allowFragments)
            
            if let registro = json["estado"] as? Int {
                if registro == 1 {
                    
                    let apiKey = json["APIkey"] as! String
                    //saber si es la primera vez o no
                    let defaults = UserDefaults.standard
                    defaults.set(apiKey, forKey: "ApiKey")
                    defaults.setObject(FBSDKProfile.currentProfile().name!, forKey: "NombreUsuario")
                    
                    DispatchQueue.main.async(execute: { // swift 3, This application is modifying the autolayout engine from a background thread, which can lead to engine corruption and weird crashes.  This will cause an exception in a future release.
                        
                        let alert = UIAlertView(title: "", message: "Te regalamos SALDO para tus primeras impresiones, después podrás recargar en las sucursales o por PayPal*.", delegate: nil, cancelButtonTitle: "OK")
                        alert.show()
                        
                        self.performSegue(withIdentifier: "PrincipalSegue", sender: nil)
                    })
                    
                } else if registro == 3 {
                    
                    DispatchQueue.main.async(execute: { // swift 3, This application is modifying the autolayout engine from a background thread, which can lead to engine corruption and weird crashes.  This will cause an exception in a future release.
                        
                        let alert = UIAlertView(title: "Usuario ya registrado", message: "Ya te registraste anteriormente.", delegate: nil, cancelButtonTitle: "OK")
                        alert.show()
                        
                        if FBSDKProfile.currentProfile() != nil {
                            FBSDKLoginManager().logOut()
                        }
                    })
                    
                }
            }
            
        } catch {
            print("error serializing JSON: \(error)")
        }
        
    }
    
    func validate(_ value: String?) -> Bool {
        //si es nil doThis si no doThat
        //value == nil ? doThis(): doThat()
                
        let PHONE_REGEX = "^55\\d{8}"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        
        return result
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func swipeKeyBoard(_ sender:AnyObject) {
        //Baja el textField
        self.view.endEditing(true)
    }
    
    
    // MARK: - FBSDKLoginButtonDelegate
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        
        if error != nil {
            // Process error
            if FBSDKProfile.currentProfile() != nil {
                FBSDKLoginManager().logOut()
            }
        }
        else if result.isCancelled {
            // Handle cancellations
        }
        else {
            // Navigate to other view
            
            if loginButton.titleLabel?.text == "Registrarte" {
                print("Register")
                
                let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"name, email"])
                graphRequest.start(completionHandler: { (connection, result, error) -> Void in
                    
                    if ((error) != nil)
                    {
                        // Process error
                        print("Error: \(error)")
                    }
                    else
                    {
                        print("fetched user: \(result)")
                        //let userName : NSString = result.valueForKey("name") as! NSString
                        let userEmail = result.valueForKey("email") as! String
                        
                        self.alertRegistro(userEmail)
                    }
                })
                
                //self.performSegueWithIdentifier("RegistroSegue", sender: nil)
                
            } else if loginButton.titleLabel?.text == "Iniciar Sesión" {
                print("logIN")
                self.loginHurry()
            }
        }
        
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("logOut")
    }*/

}

