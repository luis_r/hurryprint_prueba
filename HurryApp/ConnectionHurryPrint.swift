//
//  HTTPPostToPHP.swift
//  HurryPrint
//
//  Created by Emmanuel Valentín Granados López on 17/12/15.
//  Copyright © 2015 DevWorms. All rights reserved.
//

import Foundation

class ConnectionHurryPrint {
    
    var task : URLSessionDataTask?
    
    func prepareBodyDataToPHP(_ parameters: [String: String], boundary: String) -> NSMutableData {
        
        // Set Content-Type in HTTP header.
        // http://www.sitepoint.com/web-foundations/mime-types-complete-list/
        let mimeType = "application/pdf"
        let fieldName = "documento" // $_FILES
        
        let requestBodyData = NSMutableData()
        
        for (keys, values) in parameters {
            
            requestBodyData.appendString("--\(boundary)\r\n")
            requestBodyData.appendString("Content-Disposition: form-data; name=" + keys + "\r\n\r\n")
            requestBodyData.appendString("\( values )\r\n")  // numero puede ser string o integer
            print("\(keys): \(values)")
        }
        
        requestBodyData.appendString("--\(boundary)\r\n")
        requestBodyData.appendString("Content-Disposition: form-data; name=\"\( "llave" )\"\r\n\r\n")
        requestBodyData.appendString("\( UserDefaults.standard.string(forKey: "ApiKey")! )\r\n")  // numero puede ser string o integer
        
        requestBodyData.appendString("--\(boundary)\r\n")
        requestBodyData.appendString("Content-Disposition: form-data; name=\"\(fieldName)\"; filename=" + ( MyFile.Name ) + "\r\n")
        requestBodyData.appendString("Content-Type: \(mimeType)\r\n\r\n")
        requestBodyData.append( try! Data(contentsOf: URL(fileURLWithPath: MyFile.Path) ) )
        requestBodyData.appendString("\r\n")
        requestBodyData.appendString("--\(boundary)--\r\n")
        
        return requestBodyData
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
    
    //MARK: - Completion Handler
    
    func connectionRestApi(_ url: String, type: String, headers: [String: String]?, parameters: [String: String]?, completion: @escaping (_ resultData: Data) -> Void) {
        
        if Accesibilidad.isConnectedToNetwork() == true {
            let request = NSMutableURLRequest(url: URL(string: url)!)
            request.allHTTPHeaderFields = headers
            
            
            if type == "GET" {
                request.httpMethod = type
                
                request.addValue( UserDefaults.standard.string(forKey: "ApiKey")! , forHTTPHeaderField: "Apikey")
                
            } else if type == "POST" { // "POST1"
            
                do {
                    let postData = try JSONSerialization.data(withJSONObject: parameters!, options: .prettyPrinted)
                    
                    request.httpMethod = type
                    
                    request.httpBody = postData
                    
                } catch {
                    print("error serializing JSON: \(error)")
                }
            } else {
                
                let boundary = generateBoundaryString()
                
                // Set Content-Type in HTTP header.
                let contentType = "multipart/form-data; boundary=" + boundary
                
                request.httpMethod = "POST"
                
                // Set the HTTPBody we'd like to submit
                request.httpBody = self.prepareBodyDataToPHP(parameters!, boundary: boundary) as Data
                
                request.setValue(contentType, forHTTPHeaderField: "Content-Type")
                
                
            }
            
            
            self.task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if error != nil {
                    print("error Request =\(error)")
                    return
                } else {
                    
                    let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("responseString = \(responseString)")
                    
                    completion(data!)
                    
                }
                
            }
            self.task!.resume()
            
        } else {
            let alert = UIAlertView(title: "Sin conexión a internet", message: "Asegúrate de estar conectado a internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }
    
    func cancelConnection() {
        
        if self.task != nil {
            self.task!.cancel()
        }
        
    }
    
}

extension NSMutableData {
    
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
