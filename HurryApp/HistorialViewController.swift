//
//  HistorialViewController.swift
//  HurryPrint
//
//  Created by Emmanuel Valentín Granados López on 13/01/16.
//  Copyright © 2016 DevWorms. All rights reserved.
//

import UIKit

class HistorialViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    var dateFormatter = DateFormatter()
    
    var hurryPrintMethods = ConnectionHurryPrint()
    var numberRows = 0
    var names = [String]()
    var stats = [String]()
    var noFolios = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // set up the refresh control
        //https://grokswift.com/pull-to-refresh-swift-table-view/
        self.dateFormatter.dateStyle = DateFormatter.Style.short
        self.dateFormatter.timeStyle = DateFormatter.Style.long
        
        self.refreshControl.attributedTitle = NSAttributedString(string: "Actualizando...")
        self.refreshControl.addTarget(self, action: #selector(HistorialViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        self.tableView.addSubview( self.refreshControl )
        
        self.loadStockQuoteItems()
    }
    
    func refresh(_ sender:AnyObject) {
        self.loadStockQuoteItems()
    }
    
    func loadStockQuoteItems() {
        
        // update "last updated" title for refresh control
        let now = Date()
        let updateString = "Última actualización " + self.dateFormatter.string(from: now)
        self.refreshControl.attributedTitle = NSAttributedString(string: updateString)
        
        // tell refresh control it can stop showing up now
        if self.refreshControl.isRefreshing {
            self.refreshControl.endRefreshing()
        }
        
        //Completion Handler
        self.hurryPrintMethods.connectionRestApi( "http://hurryprint.devworms.com/api/folios", type: "GET", headers: nil, parameters: nil, completion: { (resultData) -> Void in
            
            self.parseJSON( resultData )
            
            DispatchQueue.main.async(execute: {
                
                self.tableView.reloadData()
            })
            
        })
        
    }
    
    func parseJSON(_ dataForJson: Data) {
        do {
            let json = try JSONSerialization.jsonObject(with: dataForJson, options: []) as! [String: AnyObject]
            
            if let folios = json["folios"] as? [[String: AnyObject]] {
                for folio in folios {
                    
                    if let noFolio = folio["folio_documento"] as? String {
                        self.noFolios.append(noFolio)
                    }
                    if let name = folio["nombre_documento"] as? String {
                        self.names.append(name)
                    }
                    if let estatus = folio["descripcion"] as? String {
                        self.stats.append(estatus)
                    }
                }
                
                if folios.count > 0 {
                    self.numberRows = folios.count
                }
            }
        } catch {
            print("error serializing JSON: \(error)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if numberRows == 0 {
            
            let noDataLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: tableView.bounds.height))
            noDataLabel.numberOfLines = 2
            noDataLabel.font = UIFont.boldSystemFont(ofSize: 16)
            noDataLabel.textColor = UIColor.black
            noDataLabel.textAlignment = NSTextAlignment.center
            noDataLabel.backgroundColor = UIColor.clear//UIColor.lightGrayColor()
            noDataLabel.text = "No tienes productos por recoger"
            
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = UITableViewCellSeparatorStyle.none
                        
        } else {
        
            tableView.backgroundView = nil
            tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        }
        
        return self.numberRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellHistorial", for: indexPath) as UITableViewCell
        
        let nameDocumento = cell.viewWithTag(1) as! UILabel
        nameDocumento.text = self.names[ indexPath.row ]
        
        let folio = cell.viewWithTag(2) as! UILabel
        folio.text = self.noFolios[ indexPath.row ]
        
        let estatus = cell.viewWithTag(3) as! UILabel
        estatus.text = self.stats[ indexPath.row ]
        
        if self.self.stats[indexPath.row] != "esperando" {
            estatus.textColor = UIColor.green
        }
        
        return cell
        
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "FolioSegue", let destination = segue.destination as? InfoHistorialViewController {
            if let cell = sender as? UITableViewCell, let indexPath = self.tableView.indexPath(for: cell) {
                
                destination.folioSearch = self.noFolios[ indexPath.row ]
            }
        }
    }

}
